# Trychess

Finds all unique configurations of a set of normal chess pieces on 
a chess board with dimensions M×N where none of the pieces is in a position to take any of the 
others. The colour of the piece does not matter, and there are no pawns among the pieces. 


### Build:


```
$ mvn clean package shade:shade -DskipTests
```


### Usage:

```
$ java -jar <PATH_TO_YOUR_JAR>/trychess-1.0-SNAPSHOT.jar M×N Pieces
```

#### Options:

```
-f format, 'simple' or 'inline'. Default is 'simple'
-o file to save the results (optional)
```

**Example:**  
```
$ java -jar <PATH_TO_YOUR_JAR>/trychess-1.0-SNAPSHOT.jar 3x3 KKR
```

will write to the console

```
4 combinations found in 9 millis
|K|·|K|
|·|·|·|
|·|R|·|

|·|R|·|
|·|·|·|
|K|·|K|

|·|·|K|
|R|·|·|
|·|·|K|

|K|·|·|
|·|·|R|
|K|·|·|
```

To save the result to a file, use the `-o` option with the path of the file to be created:

```
$ java -jar <PATH_TO_YOUR_JAR>/trychess-1.0-SNAPSHOT.jar 3x3 KKR -o /tmp/kkr.txt -f inline
```
