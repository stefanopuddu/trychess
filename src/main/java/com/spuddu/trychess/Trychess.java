package com.spuddu.trychess;

import java.io.File;
import java.io.PrintStream;

import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.cmdline.TrychessArgs;
import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.util.print.board.BoardPrinter;


public class Trychess {

	private final static Logger logger = LoggerFactory.getLogger(Trychess.class);

	public static void main(String[] args) throws Throwable {
		TrychessArgs chessArgs = new TrychessArgs();
		CmdLineParser parser = new CmdLineParser(chessArgs);
		try {
			parser.parseArgument(args);
			chessArgs.validate();
			
			Board board = chessArgs.getBoard();
			String pieces = chessArgs.getPieces();

			long start = System.currentTimeMillis();
			board.solve(pieces);
			long end = System.currentTimeMillis();

			printResult(chessArgs.getOut(), chessArgs.getPrinter(), board, start, end);
		} catch (final Throwable t) {
			parser.printUsage(System.out);
			throw t;
		}
	}

	protected static void printResult(File out, BoardPrinter printer, Board board, long start, long end) {
		PrintStream printStream = System.out;
		if (out == null) {		
			printer.printResult(printStream, board, start, end, true);
		} else {
			printer.printResult(board, start, end, out.getAbsolutePath());
		}
	}

}
