package com.spuddu.trychess.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Can be placed? 
 *
 */
public class PlacementInfo {
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}

	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	public List<Position> getBoundaries() {
		return boundaries;
	}
	public void setBoundaries(List<Position> boundaries) {
		this.boundaries = boundaries;
	}

	private boolean success;
	private List<Position> boundaries = new ArrayList<Position>();
	private Position position;
	private Piece piece;

}
