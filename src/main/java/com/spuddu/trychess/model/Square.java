package com.spuddu.trychess.model;

/**
 * A {@link Grid} element
 *
 */
public class Square {

	@Override
	public Square clone() {
		Square clone = new Square(this.getRow(), this.getColumn());
		clone.setValue(new String(this.getValue()));
		return clone;
	}

	public Square(int row, int column) {
		super();
		this.setPosition(new Position(row, column));
	}
	
	public int getRow() {
		return this.getPosition().getRow();
	}

	public int getColumn() {
		return this.getPosition().getCol();
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}

	private Position position;
	private String value = EMPTY;
	
	public static final String EMPTY = "";
	public static final String PATH = "·";
	
}
