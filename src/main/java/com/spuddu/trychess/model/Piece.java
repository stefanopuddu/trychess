package com.spuddu.trychess.model;

public interface Piece {

	public String getSymbol();
	
	public PlacementInfo getPlacementInfo(Grid grid, int row, int col);

	public static final String KING = "K";
	
	public static final String ROOK = "R";

	public static final String KNIGHT = "N";

	public static final String BISHOP = "B";

	public static final String QUEEN = "Q";
}
