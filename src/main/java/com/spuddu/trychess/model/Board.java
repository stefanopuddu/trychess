package com.spuddu.trychess.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.piece.PieceFactory;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;

/**
 * The chess board
 *
 */
public class Board {

	private final static Logger logger = LoggerFactory.getLogger(Board.class);

	public Board(int numRows, int numColumns) {
		super();
		this.numRows = numRows;
		this.numColumns = numColumns;
	}

	public void solve(String textInput) {
		List<Piece> input = PieceFactory.createPieceList(textInput);
		Grid grid = this.getGridInstance();
		this.prepareGrid(input, grid, 0, 0);
	}

	public void prepareGrid(List<Piece> input, Grid grid, int row, int col) {
		Piece piece = input.get(0);
		List<PlacementInfo> placements = this.getAllPlacementInfo(grid, piece, row, col);
		for (int placInfo = 0; placInfo < placements.size(); placInfo++)  {
			PlacementInfo placementInfo = placements.get(placInfo);
			if (null != placementInfo && placementInfo.isSuccess()) {
				Grid clone = grid.clone();
				this.putInGrid(input, piece, clone, placementInfo);
			} else {
				logger.warn("this should not happen!");
			}
		}
	}

	public void putInGrid(List<Piece> input, Piece piece, Grid clone, PlacementInfo placementInfo) {
		clone = this.put(clone, placementInfo);
		List<Piece> newIn = new ArrayList<Piece>();
		newIn.addAll(input);
		newIn.remove(piece);
		if (newIn.isEmpty()) {
			this.addValidCombination(clone);
		} else {
			PlacementInfo next = this.findPosition(clone, newIn.get(0), placementInfo.getPosition().getRow(), placementInfo.getPosition().getCol());
			if (null != next) {
				this.prepareGrid(newIn, clone, next.getPosition().getRow(), next.getPosition().getCol());
			}
		}
	}
	

	/**
	 * Returns a clean {@link Grid}
	 * @return
	 */
	public Grid getGridInstance() {
		return new Grid(this.getNumRows(), this.getNumColumns());
	}


	public Grid put(Grid grid, PlacementInfo placementInfo) {
		if (!placementInfo.isSuccess()) {
			
			logger.warn("invalid put! Cannot place {} in {},{} -\n{}", placementInfo.getPiece().getSymbol(), placementInfo.getPosition().getRow(), placementInfo.getPosition().getCol(), BoardPrinterFactory.getPrinter(BoardPrinter.SIMPLE).print(grid));
			return grid;
		}
		Grid clone = grid.clone();
		Iterator<Position> it = placementInfo.getBoundaries().iterator();
		while (it.hasNext()) {
			Position coord = it.next();
			clone.getSquare(coord.getRow(), coord.getCol()).setValue(Square.PATH);
		}
		clone.getSquare(placementInfo.getPosition().getRow(), placementInfo.getPosition().getCol()).setValue(new String(placementInfo.getPiece().getSymbol()));
		return clone;
	}

	/**
	 * utility method. not involved in the main process
	 * @param grid
	 * @return
	 */
	public List<Position> getAllFreePositions(Grid grid) {
		List<Position> result = new ArrayList<Position>();
		for (int r = 0; r < grid.getNumRows() ; r++) { // 
			for (int c = 0; c < grid.getNumColumns(); c++) {
				Position p = new Position(r,c);
				if (grid.getSquare(r, c).getValue().equalsIgnoreCase(Square.EMPTY)) {
					result.add(p);
				}
			}
		}
		return result;
	}

	/**
	 * Get the next available position in the grid, starting from row,column excluded
	 * May return the current position
	 * @param grid
	 * @param row
	 * @param col
	 */
	public Position getNextFreePosition(Grid grid, int row, int col) {
		Position position = null;
		Integer[] res = this.scanRow(row, col, grid, new Position(row, col));
		if (null != res) {
			position = new Position(res[0], res[1]);
		} else {
			List<Integer> rowsToScan = this.getRowsToScan(row, col);
			//search other rows
			for (int r = 0; r < rowsToScan.size(); r++) {
				res = this.scanRow(rowsToScan.get(r), 0, grid, null);
				if (null != res) {
					position = new Position(res[0], res[1]);
					break;
				}
			}
		}
		if (null != position) {
			logger.trace("next position from {},{} :{},{}", row, col, position.getRow(), position.getCol());
		} else {
			logger.trace("no next position found ({},{})", row, col);			
		}
		return position;
	}

	private Integer[] scanRow(int r, int col, Grid grid, Position current) {
		Integer[] result = null;
		for (int c = col; c < grid.getNumColumns(); c++) {
			Square square = grid.getSquare(r, c);
			boolean isStartPos = null != current && current.equals(square.getPosition());
			if (!isStartPos && square.getValue().equalsIgnoreCase(Square.EMPTY)) {
				result = new Integer[]{r, c};
				break;
			}
		}
		return result;
	}

	private List<Integer> getRowsToScan(int rowIndex, int colIndex) {
		List<Integer> rows = new ArrayList<Integer>();
		for (int i = rowIndex+1; i < this.getNumRows(); i++) {
			rows.add(i);
		}
		for (int i = 0; i < rowIndex; i++) {
			rows.add(i);
		}
		if (colIndex < this.getNumColumns()) {
			rows.add(rowIndex);
		}
		return rows;
	}

	/**
	 * Finds all the suitable placements for a piece.
	 * @param grid the target grid
	 * @param piece the piece to place
	 * @param row the row index to start the search
	 * @param col the column index to start the search
	 * @return a list with all the valid placements. The list could be empty.
	 */
	public List<PlacementInfo> getAllPlacementInfo(Grid grid, Piece piece, int row, int col) {
		List<PlacementInfo> result = new ArrayList<PlacementInfo>();
		Position start = new Position(row, col);
		if (!grid.getSquare(row, col).getValue().equalsIgnoreCase(Square.EMPTY)) {
			logger.debug("this should not happen! the start position is not free");
			start = this.getNextFreePosition(grid, row, col);
			if (null == start) return result;
		}
		List<Position> tmpPos = new ArrayList<Position>();
		PlacementInfo info = this.findPosition(grid, piece, start.getRow(), start.getCol());
		if (null != info) {
			result.add(info);
			tmpPos.add(info.getPosition());
			Position next = this.getNextFreePosition(grid, info.getPosition().getRow(), info.getPosition().getCol());
			while (null != next) {
				PlacementInfo p2 = this.findPosition(grid, piece, next.getRow(), next.getCol());
				if (null == p2 || tmpPos.contains(p2.getPosition())) {
					break;
				}
				result.add(p2);
				tmpPos.add(p2.getPosition());
				next = this.getNextFreePosition(grid, p2.getPosition().getRow(), p2.getPosition().getCol());
			}
		}
		return result;
	}

	/**
	 * Finds the first suitable placement for a piece
	 * @param grid the target grid
	 * @param currentPiece the piece to place
	 * @param row the row index to start the search
	 * @param col the column index to start the search
	 * @return a valid placement info or null if the piece can not be placed in this grid
	 */
	public PlacementInfo findPosition(Grid grid, Piece currentPiece, int row, int col) {
		return this.findPosition(grid, currentPiece, new ArrayList<Position>(), row, col);
	}

	private PlacementInfo findPosition(Grid grid, Piece currentPiece, List<Position> tentativi, int row, int col) {
		PlacementInfo placementInfo = currentPiece.getPlacementInfo(grid, row, col);
		if (placementInfo.isSuccess()) {
			return placementInfo;
		} else {
			Position next = this.getNextFreePosition(grid, row, col);
			if (null != next && !tentativi.contains(next)) {
				tentativi.add(next);
				PlacementInfo info = this.findPosition(grid, currentPiece, tentativi, next.getRow(), next.getCol());
				if (null != info && info.isSuccess()) {
					return info;
				}
			} else {
				//logger.trace("non more positions...");
			}
		}
		return null;
	}

	/**
	 * Returns a list containing the unique permutations for the text input
	 * @param input
	 * @return
	 */
	public Set<String> getInputPermutations(String input) {
		Set<String> result = permutation(input);
		return result;
	}

	private static Set<String> permutation(String str) {
		Set<String> result = new HashSet<String>();
		permutation("", str, result); 
		return result;
	}
	private static void permutation(String prefix, String str, Set<String> result) {
		int n = str.length();
		if (n == 0) result.add(prefix);
		else {
			for (int i = 0; i < n; i++)
				permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n), result);
		}
	}

	//TODO FORMAT!
	public void addValidCombination(Grid grid) {
		StringBuffer sbBuffer = new StringBuffer();
		for (int row = 0; row < this.getNumRows(); row++) {
			for (int col = 0; col < this.getNumColumns(); col++) {
				Square square = grid.getSquare(row, col);
				if (!square.getValue().equalsIgnoreCase(Square.EMPTY) && !square.getValue().equalsIgnoreCase(Square.PATH)) {
					if (sbBuffer.length() > 0) sbBuffer.append(COMB_ELEMENT_SEPARATOR);
					String combPos = row + COMB_ATTR_SEPARATOR + col + COMB_ATTR_SEPARATOR +square.getValue();
					sbBuffer.append(combPos);
				}
			}
		}
		validCombinations.add(sbBuffer.toString());
	}

	public Grid decodeCombination(String combination) {
		Grid grid = this.getGridInstance();
		String[] positions = combination.split(COMB_ELEMENT_SEPARATOR);
		for (int i = 0; i < positions.length; i++) {
			PlacementInfo placementInfo = this.createPlacement(grid, positions[i]);
			grid = this.put(grid, placementInfo);
		}
		return grid;
	}

	private PlacementInfo createPlacement(Grid grid, String combPosition) {
		String[] comb = combPosition.split(COMB_ATTR_SEPARATOR);
		int r = new Integer(comb[0]);
		int c = new Integer(comb[1]);
		Piece p = PieceFactory.getPiece(comb[2]);
		PlacementInfo placementInfo = p.getPlacementInfo(grid, r, c);
		return placementInfo;
	}

	public int getNumRows() {
		return numRows;
	}
	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	public int getNumColumns() {
		return numColumns;
	}
	public void setNumColumns(int numColumns) {
		this.numColumns = numColumns;
	}


	public Set<String> getValidCombinations() {
		return validCombinations;
	}
	public void setValidCombinations(Set<String> validCombinations) {
		this.validCombinations = validCombinations;
	}

	private int numRows;
	private int numColumns;
	private Set<String> validCombinations = new HashSet<String>();


	private static final String COMB_ELEMENT_SEPARATOR = ";";
	private static final String COMB_ATTR_SEPARATOR = ",";

}
