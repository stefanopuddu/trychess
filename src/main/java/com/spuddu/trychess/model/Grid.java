package com.spuddu.trychess.model;


public class Grid {

	private Square[][] squares;
	private int numRows;
	private int numColumns;

	public Grid(int rows, int cols) {
		this.numRows = rows;
		this.numColumns = cols;
		this.squares = this.createSquares();
	}
	
	private Square[][] createSquares() {
		Square[][] grid = new Square[this.getNumRows()][this.getNumColumns()];
		for (int r = 0; r < this.getNumRows(); r++) {
			for (int c = 0; c < this.getNumColumns(); c++) {
				grid[r][c] = new Square(r, c);
			}
		}
		return grid;
	}
	
	@Override
	public Grid clone() {
		Grid clone = new Grid(this.getNumRows(), this.getNumColumns());
		for (int row = 0; row < this.getNumRows(); row++) {
			for (int col = 0; col < this.getNumColumns(); col++) {
				clone.setSquare(row, col, this.getSquare(row, col).clone());
			}
		}
		return clone;
	}
	
	public void setSquare(int row, int col, Square square) {
		this.squares[row][col] = square;
		
	}
	public Square getSquare(int row, int col) {
		return this.squares[row][col];
	}
	
	public Square[] getRow(int rowIndex) {
		return this.squares[rowIndex];
	}

	public Square[] getColumn(int columnIndex) {
		Square[] result = new Square[this.getNumRows()];
		for (int i = 0; i < result.length; i++) {
			result[i] = this.getSquare(i, columnIndex);
		}
		return result;
	}
	
	protected Square[][] getSquares() {
		return squares;
	}
	protected void setSquares(Square[][] squares) {
		this.squares = squares;
	}

	public int getNumRows() {
		return numRows;
	}
	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	public int getNumColumns() {
		return numColumns;
	}
	public void setNumColumns(int numColumns) {
		this.numColumns = numColumns;
	}
}
