package com.spuddu.trychess.model.piece;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.PlacementInfo;
import com.spuddu.trychess.model.Position;
import com.spuddu.trychess.model.Square;

public abstract class AbstractPiece implements Piece {

	private final static Logger logger = LoggerFactory.getLogger(AbstractPiece.class);
	
	public PlacementInfo getPlacementInfo(Grid grid, int row, int col) {
		PlacementInfo placementInfo = new PlacementInfo();
		Square target = grid.getSquare(row, col);
		placementInfo.setPosition(new Position(row, col));
		placementInfo.setPiece(this);

		if (!target.getValue().equalsIgnoreCase(Square.EMPTY)) {
			placementInfo.setSuccess(false);
			logger.trace("the square {}{} is not empty: {}", row, col, target.getValue());
			return placementInfo;
		}

		List<Position> boundaries = this.extractBoundaries(grid, row, col);
		if (null == boundaries) {
			placementInfo.setSuccess(false);
			logger.trace("boundaries null!");
			return placementInfo;
		}
		
		placementInfo.setBoundaries(boundaries);
		placementInfo.setSuccess(true);
		
		return placementInfo;
	}
	
	/**
	 * Return all the possible squares where this piece can move 
	 * @param grid the target grid
	 * @param row the row index where the {@link Piece} is going to be placed
	 * @param col the column index where the {@link Piece} is going to be placed
	 * @return
	 */
	protected List<Position> extractBoundaries(Grid grid, int row, int col) {
		List<Position> squares = new ArrayList<Position>();
		List<Position> tmpSquares = new ArrayList<Position>();

		this.extractPositions(grid, row, col, tmpSquares);

		Iterator<Position> it = tmpSquares.iterator();
		while (it.hasNext()) {
			Position pos = it.next();
			Square current = grid.getSquare(pos.getRow(), pos.getCol());
			if (!(current.getValue().equalsIgnoreCase(Square.EMPTY) || current.getValue().equalsIgnoreCase(Square.PATH))) {
				logger.trace("threaten raised in {},{}", pos.getRow(), pos.getCol());
				return null;
			}
			squares.add(pos);
		}
		return squares;
	}

	/**
	 * 
	 * @param grid
	 * @param row
	 * @param col
	 * @param tmpSquares
	 */
	protected abstract void extractPositions(Grid grid, int row, int col, List<Position> tmpSquares);

}
