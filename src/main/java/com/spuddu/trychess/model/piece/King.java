package com.spuddu.trychess.model.piece;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.Position;
import com.spuddu.trychess.model.Square;

/**
 * <pre>
 * | | | | | |
 * | |·|·|·| |
 * | |·|K|·| |
 * | |·|·|·| |
 * | | | | | |
 * </pre>
 *
 */
public class King extends AbstractPiece implements Piece {

	private final static Logger logger = LoggerFactory.getLogger(King.class);

	@Override
	public String getSymbol() {
		return Piece.KING;
	}

	@Override
	protected void extractPositions(Grid grid, int row, int col, List<Position> tmpSquares) {
		int minX = row-1 >= 0 ? row - 1: 0;
		int minY = col-1 >= 0 ? col - 1: 0;

		int maxX = row + 1 < grid.getNumRows() - 1 ? row + 1 : grid.getNumRows() - 1;
		int maxY = col + 1 < grid.getNumColumns() - 1 ? col + 1 : grid.getNumColumns() - 1;

		for (int r = minX; r <= maxX; r++) {
			for (int c = minY; c <= maxY; c++) {
				Square current = grid.getSquare(r, c);
				if (current.getRow() != row || current.getColumn() != col) {
					tmpSquares.add(new Position(r, c));
				}
			}
		}
	}

}
