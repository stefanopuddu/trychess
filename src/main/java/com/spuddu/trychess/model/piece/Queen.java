package com.spuddu.trychess.model.piece;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.Position;

/**
 * <pre>
 * |·| |·| |·|
 * | |·|·|·| |
 * |·|·|Q|·|·|
 * | |·|·|·| |
 * |·| |·| |·|
 * </pre>
 *
 */
public class Queen extends AbstractPiece implements Piece {

	private final static Logger logger = LoggerFactory.getLogger(Queen.class);

	@Override
	public String getSymbol() {
		return Piece.QUEEN;
	}

	@Override
	protected void extractPositions(Grid grid, int row, int col, List<Position> tmpSquares) {
		//NW
		this.extractPositionsNW(row, col, tmpSquares);
		//NE
		this.extractPositionsNE(grid, row, col, tmpSquares);
		//SW
		extractPositionsSW(grid, row, col, tmpSquares);
		//SE
		this.extractPositionsSE(grid, row, col, tmpSquares);
		//H
		this.extractPositionsWE(grid, row, tmpSquares);
		//V
		this.extractPositionsNS(grid, col, tmpSquares);

		boolean remove = tmpSquares.remove(new Position(row, col));
		while (remove) {
			remove = tmpSquares.remove(new Position(row, col));
		}
	}

	private void extractPositionsNS(Grid grid, int col,	List<Position> tmpSquares) {
		int rowIndex = 0;
		int colIndex = col;
		while (rowIndex < grid.getNumRows()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex++;
		}
	}

	private void extractPositionsWE(Grid grid, int row,	List<Position> tmpSquares) {
		int rowIndex = row;
		int colIndex = 0;
		while (colIndex < grid.getNumColumns()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			colIndex++;
		}
	}

	private void extractPositionsSE(Grid grid, int row, int col, List<Position> tmpSquares) {
		int rowIndex = row+1;
		int colIndex = col+1;
		while (rowIndex < grid.getNumRows() && colIndex < grid.getNumColumns()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex++;
			colIndex++;
		}
	}

	private void extractPositionsSW(Grid grid, int row, int col,List<Position> tmpSquares) {
		int rowIndex = row+1;
		int colIndex = col-1;
		while (rowIndex < grid.getNumRows() && colIndex >=0) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex++;
			colIndex--;
		}
	}

	private void extractPositionsNE(Grid grid, int row, int col, List<Position> tmpSquares) {
		int rowIndex = row-1;
		int colIndex = col+1;
		while (rowIndex >=0 && colIndex < grid.getNumColumns()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex--;
			colIndex++;
		}
	}

	private void extractPositionsNW(int row, int col, List<Position> tmpSquares) {
		int rowIndex = row-1;
		int colIndex = col-1;
		while (rowIndex >=0 && colIndex >=0) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex--;
			colIndex--;
		}
	}

}
