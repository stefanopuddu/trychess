package com.spuddu.trychess.model.piece;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.Position;

/**
 * <pre>
 * |·| | | |·|
 * | |·| |·| |
 * | | |B| | |
 * | |·| |·| |
 * |·| | | |·|
 * <pre>
 *
 */
public class Bishop extends AbstractPiece implements Piece {

	private final static Logger logger = LoggerFactory.getLogger(Bishop.class);

	@Override
	public String getSymbol() {
		return Piece.BISHOP;
	}

	@Override
	protected void extractPositions(Grid grid, int row, int col, List<Position> tmpSquares) {
		//NW
		this.extractPositionsNW(row, col, tmpSquares);
		//NE
		this.extractPositionsNE(grid, row, col, tmpSquares);
		//SW
		this.extractPositionsSW(grid, row, col, tmpSquares);
		//SE
		this.extractPositionsSE(grid, row, col, tmpSquares);
	}

	private void extractPositionsSE(Grid grid, int row, int col, List<Position> tmpSquares) {
		int rowIndex = row+1;
		int colIndex = col+1;
		while (rowIndex < grid.getNumRows() && colIndex < grid.getNumColumns()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex++;
			colIndex++;
		}
	}

	private void extractPositionsSW(Grid grid, int row, int col, List<Position> tmpSquares) {
		int rowIndex = row+1;
		int colIndex = col-1;
		while (rowIndex < grid.getNumRows() && colIndex >=0) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex++;
			colIndex--;
		}
	}

	private void extractPositionsNE(Grid grid, int row, int col, List<Position> tmpSquares) {
		int rowIndex = row-1;
		int colIndex = col+1;
		while (rowIndex >=0 && colIndex < grid.getNumColumns()) {
			tmpSquares.add(new Position(rowIndex,colIndex));
			rowIndex--;
			colIndex++;
		}
	}

	private void extractPositionsNW(int row, int col, List<Position> tmpSquares) {
		int rowIndex = row-1;
		int colIndex = col-1;
		while (rowIndex >=0 && colIndex >=0) {
			tmpSquares.add(new Position(rowIndex, colIndex));
			rowIndex--;
			colIndex--;
		}
	}
	
}
