package com.spuddu.trychess.model.piece;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Piece;

public class PieceFactory {

	private final static Logger logger = LoggerFactory.getLogger(PieceFactory.class);

	/**
	 * Convert the input String into a list of {@link Piece}
	 * @param input the String to convert
	 * @return a list of pieces
	 */
	public static List<Piece> createPieceList(String input) {
		List<Piece> combination = new ArrayList<Piece>();
		char[] symbol = input.toCharArray();
		for (int i = 0; i < symbol.length; i++) {
			Piece piece = PieceFactory.getPiece(symbol[i]);
			if (null == piece) {
				logger.error("Invalid symbol: '{}'", symbol[i]);
				throw new RuntimeException("Invalid symbol: " + symbol[i]);
			}
			combination.add(piece);
		}
		return combination;
	}
	
	public static Piece getPiece(String symbol) {
		if(symbol == null || symbol.trim().length() == 0) {
			return null;
		}		
		if(symbol.equalsIgnoreCase(Piece.KING)){
			return new King();
		} else if(symbol.equalsIgnoreCase(Piece.KNIGHT)) {
			return new Knight();
		} else if(symbol.equalsIgnoreCase(Piece.ROOK)) {
			return new Rook();
		} else if(symbol.equalsIgnoreCase(Piece.BISHOP)) {
			return new Bishop();
		} else if(symbol.equalsIgnoreCase(Piece.QUEEN)) {
			return new Queen();
		}
		return null;
	}

	public static Piece getPiece(char c) {
		return getPiece(Character.toString(c));
	}

}
