package com.spuddu.trychess.model.piece;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.Position;

/**
 * <pre>
 * | |·| |·| |
 * |·| | | |·|
 * | | |N| | |
 * |·| | | |·|
 * | |·| |·| |
 * </pre>
 *
 */
public class Knight extends AbstractPiece implements Piece {

	private final static Logger logger = LoggerFactory.getLogger(Knight.class);
	
	@Override
	public String getSymbol() {
		return Piece.KNIGHT;
	}

	@Override
	protected void extractPositions(Grid grid, int row, int col, List<Position> tmpSquares) {		
		this.addValidPosition(grid, tmpSquares, new Position(row-2, col-1));
		this.addValidPosition(grid, tmpSquares, new Position(row-1, col-2));

		this.addValidPosition(grid, tmpSquares, new Position(row+1, col-2));
		this.addValidPosition(grid, tmpSquares, new Position(row+2, col-1));

		this.addValidPosition(grid, tmpSquares, new Position(row+2, col+1));
		this.addValidPosition(grid, tmpSquares, new Position(row+1, col+2));

		this.addValidPosition(grid, tmpSquares, new Position(row-1, col+2));
		this.addValidPosition(grid, tmpSquares, new Position(row-2, col+1));
	}

	private void addValidPosition(Grid grid, List<Position> tmpSquares, Position pos) {
		boolean validRow = pos.getRow() >= 0 && pos.getRow() < grid.getNumRows();
		boolean validCol = pos.getCol() >= 0 && pos.getCol() < grid.getNumColumns();
		if (validCol && validRow) tmpSquares.add(pos);
	}
}
