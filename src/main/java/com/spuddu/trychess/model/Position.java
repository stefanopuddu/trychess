package com.spuddu.trychess.model;

/**
 * 
 *
 */
public class Position {

	public Position(int row, int col) { 
		this.row = row;
		this.col = col;
	}

	@Override
	public boolean equals(Object obj) {
		boolean equals = false;
		if (obj instanceof Position) {
			equals = this.getRow() == ((Position)obj).getRow() && this.getCol() == ((Position)obj).getCol();
		}
		return equals;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Position [row=").append(row).append(", col=").append(col).append("]");
		return builder.toString();
	}

	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}

	private int row;
	private int col;
}
