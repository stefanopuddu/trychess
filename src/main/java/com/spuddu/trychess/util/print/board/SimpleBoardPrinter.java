package com.spuddu.trychess.util.print.board;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Square;

public class SimpleBoardPrinter extends AbstractBoardPrinter {
	
	protected SimpleBoardPrinter() {
		
	}
	
	@Override
	public String getName() {
		return BoardPrinter.SIMPLE;
	}
	
	public StringBuffer print(Grid grid, int indent) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int r = 0; r < grid.getNumRows(); r++) {
			if (r > 0) stringBuffer.append("\n");
			for (int i = 0; i < indent; i++) {
				stringBuffer.append(" ");
			}
			stringBuffer.append(this.printRow(grid, r));
		}
		return stringBuffer;
	}
	
	@Override
	public StringBuffer print(Grid grid) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int r = 0; r < grid.getNumRows(); r++) {
			if (r > 0) stringBuffer.append("\n");
			stringBuffer.append(this.printRow(grid, r));
			//stringBuffer.append("\n");
		}
		return stringBuffer;
	}

	@Override
	public StringBuffer printRow(Grid grid, int rowIndex) {
		StringBuffer stringBuffer = new StringBuffer();
		Square[] row = grid.getRow(rowIndex);
		stringBuffer.append("|");
		for (int i = 0; i < row.length; i++) {
			if (i > 0) stringBuffer.append("|");
			stringBuffer.append(this.getSquareValue(row[i]));
		}
		stringBuffer.append("|");
		return stringBuffer;
	}

	@Override
	public String printColumn(Grid grid, int columnIndex) {
		StringBuffer stringBuffer = new StringBuffer();
		Square[] column = grid.getColumn(columnIndex);
		for (int i = 0; i < column.length; i++) {
			if (i > 0) stringBuffer.append("\n");
			stringBuffer.append("|");
			stringBuffer.append(column[i].getValue());
			stringBuffer.append("|");//.append("\n");
		}
		return stringBuffer.toString();
	}
	
	@Override
	public String getSquareValue(Square square) {
		String value = square.getValue();
		if (value.equalsIgnoreCase(Square.EMPTY)) {
			return " ";
		} else if (value.equalsIgnoreCase(Square.PATH)) {
			return "·";
		} else {
			return value;
		}
	}

}
