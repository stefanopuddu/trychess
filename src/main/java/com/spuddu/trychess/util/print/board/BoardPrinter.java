package com.spuddu.trychess.util.print.board;

import java.io.PrintStream;

import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Square;

public interface BoardPrinter {

	public String getName();
	
	public void printResult(PrintStream out, Board board, long start, long end, boolean alsoGrid);
	
	public void printResult(Board board, long start, long end, String path);
	
	public StringBuffer print(Grid grid);

	public StringBuffer printRow(Grid grid, int rowIndex);

	public String printColumn(Grid grid, int columnIndex);

	public String getSquareValue(Square square);
	
	
	public static final String SIMPLE = "simple";
	
	public static final String INLINE = "inline";
}
