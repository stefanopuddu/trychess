package com.spuddu.trychess.util.print.board;

import java.io.PrintStream;

import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Square;


public class InlineBoardPrinter extends AbstractBoardPrinter {
	
	protected InlineBoardPrinter() {
		
	}

	
	@Override
	public String getName() {
		return BoardPrinter.INLINE;
	}
	
	@Override
	protected void appendSeparator(PrintStream out) {
		//do nothing
	}

	@Override
	public StringBuffer print(Grid grid) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int r = 0; r < grid.getNumRows(); r++) {
			stringBuffer.append(this.printRow(grid, r));
		}
		stringBuffer.append("#");
		return stringBuffer;
	}

	@Override
	public StringBuffer printRow(Grid grid, int rowIndex) {
		StringBuffer stringBuffer = new StringBuffer();
		Square[] row = grid.getRow(rowIndex);
		for (int i = 0; i < row.length; i++) {
			stringBuffer.append(this.getSquareValue(row[i]));
		}
		return stringBuffer;
	}

	@Override
	public String printColumn(Grid grid, int columnIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getSquareValue(Square square) {
		String value = square.getValue();
		if (value.equalsIgnoreCase(Square.EMPTY)) {
			return "-";
		} else if (value.equalsIgnoreCase(Square.PATH)) {
			return "·";
		} else {
			return value;
		}
	}
	
}
