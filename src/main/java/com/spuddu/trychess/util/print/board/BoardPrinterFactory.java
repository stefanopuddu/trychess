package com.spuddu.trychess.util.print.board;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BoardPrinterFactory {

	private final static Logger logger = LoggerFactory.getLogger(BoardPrinterFactory.class);

	public static BoardPrinter getPrinter(String name) {
		if(name == null || name.trim().length() == 0) {
			return null;
		}		
		if(name.equalsIgnoreCase(BoardPrinter.INLINE)){
			return new InlineBoardPrinter();
		} else if(name.equalsIgnoreCase(BoardPrinter.SIMPLE)) {
			return new SimpleBoardPrinter();
		} else {
			logger.warn("invalid printer name: {}", name);
		}
		return null;
	}


}
