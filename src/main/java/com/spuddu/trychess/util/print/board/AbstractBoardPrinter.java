package com.spuddu.trychess.util.print.board;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.model.Grid;

public abstract class AbstractBoardPrinter implements BoardPrinter {

	private final static Logger logger = LoggerFactory.getLogger(AbstractBoardPrinter.class);
	
	@Override
	public void printResult(PrintStream out, Board board, long start, long end, boolean alsoGrid) {
		logger.debug("{} combinations found in {} millis", board.getValidCombinations().size(), (end-start));
		if (alsoGrid) {			
			Iterator<String> it = board.getValidCombinations().iterator();
			while (it.hasNext()) {
				Grid grid = board.decodeCombination(it.next());
				out.println(this.print(grid));
				this.appendSeparator(out);
			}
		}
	}

	protected void appendSeparator(PrintStream out) {
		out.println();
	}

	@Override
	public void printResult(Board board, long start, long end, String path) {
		try {
			File dir = new File(path).getParentFile();
			if (!dir.exists()) dir.mkdirs();
			PrintStream out = new PrintStream(new FileOutputStream(path));
			this.printResult(out, board, start, end, true);
		} catch (Exception ex) {
			logger.error("Error saving file {}", path, ex);
		}
	}
}
