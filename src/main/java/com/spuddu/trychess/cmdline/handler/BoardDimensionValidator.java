package com.spuddu.trychess.cmdline.handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BoardDimensionValidator  {

	public static final String DIM_SEPARATOR = "x";
	
	private static final String INPUT_REGEX = "(\\d+)"+ DIM_SEPARATOR + "(\\d+)";

	public boolean isValid(String param) {
		if (null == param || param.trim().length() == 0) return false;
		int[] dimensions = this.parseBoardInput(param);
		if (null == dimensions ) return false;
		if (dimensions[0] == 0 || dimensions[1] == 0) return false;
		return true;
	}
	
	private int[] parseBoardInput(String param) {
		String regex = INPUT_REGEX;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(param);
		if (matcher.matches()) {
			if (matcher.groupCount() == 2) {
				int x = new Integer(matcher.group(1));
				int y = new Integer(matcher.group(2));
				return new int[] {x, y};
			}
		}
		return null;
	}
}
