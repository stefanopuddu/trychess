package com.spuddu.trychess.cmdline.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.piece.PieceFactory;

public class PiecesValidator {

	private final static Logger logger = LoggerFactory.getLogger(PiecesValidator.class);
	
	public boolean isValid(String param) {
		if (null == param || param.trim().length() == 0) return false;
		List<Piece> pieces = this.parseInput(param);
		if (null == pieces || pieces.isEmpty() ) return false;
		return true;
	}
	
	private List<Piece> parseInput(String pieceIn) {
		List<Piece> pieces = null;
		try {
			pieces = PieceFactory.createPieceList(pieceIn);
			return pieces;
		} catch (Throwable t) {
			logger.error("error parsing pieces {} ", pieceIn);
		}
		return null;
	}
}
