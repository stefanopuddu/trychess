package com.spuddu.trychess.cmdline;

import java.io.File;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.cmdline.handler.BoardDimensionValidator;
import com.spuddu.trychess.cmdline.handler.PiecesValidator;
import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;

public class TrychessArgs {

	private final static Logger logger = LoggerFactory.getLogger(TrychessArgs.class);

	@Argument(index=0, required=true, usage="The board dimension: Eg 3x3", metaVar="Board")
	private String boardDimension;

	@Argument(index=1, required=true, usage="The pieces to parse: Eg KKR", metaVar="Pieces")
	private String pieces;
	
	@Option(name="-f", usage="acceped values are 'simple' and 'inline'", metaVar="output format. Default value is 'simple'")
	private String format = BoardPrinter.SIMPLE;

	@Option(name="-o", usage="output to this file", metaVar="path to save the result")
	private File out;

	public Board getBoard() {
		String[] dim = this.getBoardDimension().split(BoardDimensionValidator.DIM_SEPARATOR);
		return new Board(new Integer(dim[0]), new Integer(dim[1]));
	}

	public void validate() throws Exception {
		BoardDimensionValidator boardDimensionValidator = new BoardDimensionValidator();
		if (!boardDimensionValidator.isValid(this.boardDimension)) {
			throw new Exception("\"" + this.getBoardDimension() + "\" is not a valid value for \"Board\"");
		}
		PiecesValidator piecesValidator = new PiecesValidator();
		if (!piecesValidator.isValid(this.pieces)) {
			throw new Exception("\"" + this.getPieces() + "\" is not a valid value for \"Pieces\"");
		}
	}
	
	/**
	 * Returns the {@link BoardPrinter} implementation according to the chosen format.
	 * @return
	 */
	public BoardPrinter getPrinter() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(this.getFormat());
		if (null == printer) {
			logger.warn("Invalid format option: '{}'. Falling back to '{}'", this.getFormat(), BoardPrinter.SIMPLE);
			printer = BoardPrinterFactory.getPrinter(BoardPrinter.SIMPLE);
		} 
		return printer;
	}

	public String getPieces() {
		return pieces;
	}
	public void setPieces(String pieces) {
		this.pieces = pieces;
	}

	protected String getBoardDimension() {
		return boardDimension;
	}
	protected void setBoardDimension(String boardDimension) {
		this.boardDimension = boardDimension;
	}

	public File getOut() {
		return out;
	}
	public void setOut(File out) {
		this.out = out;
	}

	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}


}
