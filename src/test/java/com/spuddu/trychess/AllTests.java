package com.spuddu.trychess;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.spuddu.trychess.model.TestBoard;
import com.spuddu.trychess.model.TestBoardSolution;
import com.spuddu.trychess.model.TestGridPrinter;
import com.spuddu.trychess.model.TestPlacementInfo;
import com.spuddu.trychess.model.piece.TestBishop;
import com.spuddu.trychess.model.piece.TestKing;
import com.spuddu.trychess.model.piece.TestKnight;
import com.spuddu.trychess.model.piece.TestPosition;
import com.spuddu.trychess.model.piece.TestQueen;
import com.spuddu.trychess.model.piece.TestRook;

@RunWith(Suite.class)
@SuiteClasses({ 
	TestBoard.class, 
	TestGridPrinter.class,
	TestPosition.class,
	TestPlacementInfo.class,
	
	TestBishop.class,
	TestKing.class,
	TestKnight.class,
	TestQueen.class,
	TestRook.class,
	
	TestBoardSolution.class,
	TestTrychess.class
	
	})
public class AllTests {

}
