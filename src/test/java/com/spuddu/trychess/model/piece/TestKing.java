package com.spuddu.trychess.model.piece;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.PlacementInfo;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;
import com.spuddu.trychess.util.print.board.InlineBoardPrinter;

public class TestKing {

	@Test
	public void testKing() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
		Board board = new Board(5,5);
		Grid grid = board.getGridInstance();
		King king = (King) PieceFactory.getPiece(Piece.KING);
		PlacementInfo placementInfo = king.getPlacementInfo(grid, 2, 2);
		grid = board.put(grid, placementInfo);
		assertEquals("------···--·K·--···------#", printer.print(grid).toString());
		assertEquals(8, placementInfo.getBoundaries().size());
		grid = board.getGridInstance();
		placementInfo = king.getPlacementInfo(grid, 0, 0);
		grid = board.put(grid, placementInfo);
		assertEquals(3, placementInfo.getBoundaries().size());
	}

}
