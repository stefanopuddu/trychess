package com.spuddu.trychess.model.piece;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.spuddu.trychess.model.Board;
import com.spuddu.trychess.model.Grid;
import com.spuddu.trychess.model.Piece;
import com.spuddu.trychess.model.PlacementInfo;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;
import com.spuddu.trychess.util.print.board.InlineBoardPrinter;

public class TestKnight {

	@Test
	public void testKnight() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
		Board board = new Board(5,5);
		Grid grid = board.getGridInstance();
		Knight knight = (Knight) PieceFactory.getPiece(Piece.KNIGHT);
		PlacementInfo placementInfo = knight.getPlacementInfo(grid, 2, 2);
		grid = board.put(grid, placementInfo);
		assertEquals("-·-·-·---·--N--·---·-·-·-#", printer.print(grid).toString());
		assertEquals(8, placementInfo.getBoundaries().size());
		grid = board.getGridInstance();
		placementInfo = knight.getPlacementInfo(grid, 0, 0);
		grid = board.put(board.getGridInstance(), placementInfo);
		assertEquals(2, placementInfo.getBoundaries().size());
	}
	
}
