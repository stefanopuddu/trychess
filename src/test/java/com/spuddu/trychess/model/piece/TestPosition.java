package com.spuddu.trychess.model.piece;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.spuddu.trychess.model.Position;

public class TestPosition {

	@Test
	public void testPosition() {
		Position position = new Position(3,3);
		Position other = new Position(3,3);
		assertEquals(position, other);
		
		List<Position> positions = new ArrayList<Position>();
		positions.add(new Position(0, 0));
		positions.add(new Position(0, 1));
		positions.add(new Position(0, 2));
		
		assertTrue(positions.contains(new Position(0,1)));
		assertFalse(positions.contains(new Position(1,1)));
	}
}
