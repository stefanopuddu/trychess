package com.spuddu.trychess.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;

public class TestGridPrinter {

	private final static Logger logger = LoggerFactory.getLogger(TestGridPrinter.class);

	private BoardPrinter printer;
	private BoardPrinter inlinePrinter;
	
	@Before
	public void setUp() {
		this.printer = BoardPrinterFactory.getPrinter(BoardPrinter.SIMPLE);
		this.inlinePrinter = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
	}
	
	@Test
	public void testPrintGrid() {
		Grid grid = new Board(3,3).getGridInstance();
		String out = printer.print(grid).toString();
		assertEquals("| | | |\n| | | |\n| | | |", out);
		assertEquals("---------#", inlinePrinter.print(grid).toString());
		
		grid = new Board(2,4).getGridInstance();
		out = printer.print(grid).toString();
		assertEquals("| | | | |\n| | | | |", out);
		assertEquals("--------#", inlinePrinter.print(grid).toString());

		grid = new Board(4,2).getGridInstance();
		out = printer.print(grid).toString();
		assertEquals("| | |\n| | |\n| | |\n| | |", out);
		assertEquals("--------#", inlinePrinter.print(grid).toString());
	}

	@Test
	public void testPrintColumn() {
		Board board = new Board(3, 3);
		Grid grid = board.getGridInstance();
		for (int r = 0; r < board.getNumRows(); r++) {
			for (int c = 0; c < board.getNumColumns(); c++) {
				grid.getSquare(r, c).setValue(r + ";" + c);
			}
		}
		String col = printer.printColumn(grid, 1).toString();
		assertEquals("|0;1|\n|1;1|\n|2;1|", col);
	}

	@Test
	public void testPrintRow() {
		Board board = new Board(3, 3);
		Grid grid = board.getGridInstance();
		for (int r = 0; r < board.getNumRows(); r++) {
			for (int c = 0; c < board.getNumColumns(); c++) {
				grid.getSquare(r, c).setValue("["+r + ";" + c + "]");
			}
		}
		String row = printer.printRow(grid, 1).toString();
		assertEquals("|[1;0]|[1;1]|[1;2]|", row);
		assertEquals("[1;0][1;1][1;2]", inlinePrinter.printRow(grid, 1).toString());
	}
}
