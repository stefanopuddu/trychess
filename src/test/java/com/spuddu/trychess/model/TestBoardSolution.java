package com.spuddu.trychess.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;

public class TestBoardSolution {

	private final static Logger logger = LoggerFactory.getLogger(TestBoardSolution.class);
	
	private BoardPrinter simpleBoardPrinter;
	private BoardPrinter inlineBoardPrinter;
	
	@Before
	public void setUp() {
		this.simpleBoardPrinter = BoardPrinterFactory.getPrinter(BoardPrinter.SIMPLE);
		this.inlineBoardPrinter = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
	}

	@Test
	public void testBoard_KKR() throws FileNotFoundException {
		String inputString = "KKR";
		Board board = new Board(3,3);

		long start = System.currentTimeMillis();
		board.solve(inputString);
		long end = System.currentTimeMillis();
		
		assertEquals(4, board.getValidCombinations().size());
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(stream);
		this.inlineBoardPrinter.printResult(out, board, start, end, true);
		assertEquals("K·K····R·#\n·R····K·K#\n··KR····K#\nK····RK··#\n", stream.toString());
		
		String pathGrid = "target" + File.separator + "kkr_grid.txt";
		String pathInline = "target" + File.separator + "kkr_inline.txt";
		this.simpleBoardPrinter.printResult(board, start, end, pathGrid);
		this.inlineBoardPrinter.printResult(board, start, end, pathInline);
		
		File grid = new File(pathGrid);
		assertTrue(grid.exists());
		grid.delete();
		
		File inline = new File(pathInline);
		assertTrue(inline.exists());
		inline.delete();
	}

	@Test
	public void testBoard_NNNNRR() {
		String inputString = "NNNNRR";
		Board board = new Board(4,4);
		
		long start = System.currentTimeMillis();
		board.solve(inputString);
		long end = System.currentTimeMillis();
		assertEquals(8, board.getValidCombinations().size());

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(stream);
		this.inlineBoardPrinter.printResult(out, board, start, end, true);
		assertEquals("·R··N·N····RN·N·#\n·N·NR····N·N··R·#\n···RN·N··R··N·N·#\n··R··N·NR····N·N#\nR····N·N··R··N·N#\nN·N··R··N·N····R#\n·N·N··R··N·NR···#\nN·N····RN·N··R··#\n", stream.toString());
	}

	@Test
	public void testBoard_KKQQBBK() {
		String inputString = "KKQQBBN";
		Board board = new Board(7,7);
		long start = System.currentTimeMillis();
		board.solve(inputString);
		long end = System.currentTimeMillis();
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(stream);
		
		this.inlineBoardPrinter.printResult(out, board, start, end, false);
		assertEquals(3063828, board.getValidCombinations().size());
		logger.info("{} solutions found in {} millis", board.getValidCombinations().size(), (end-start));
	}


}
