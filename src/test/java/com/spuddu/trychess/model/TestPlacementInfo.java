package com.spuddu.trychess.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spuddu.trychess.model.piece.King;
import com.spuddu.trychess.model.piece.Rook;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;
import com.spuddu.trychess.util.print.board.InlineBoardPrinter;

public class TestPlacementInfo {

	private final static Logger logger = LoggerFactory.getLogger(TestPlacementInfo.class);
	
	@Test
	public void testPlacement_rook() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
		
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		
		Piece piece = new Rook();
		PlacementInfo placementInfo = piece.getPlacementInfo(grid, 1, 2);
		assertTrue(placementInfo.isSuccess());
		List<Position> squares = placementInfo.getBoundaries();
		Iterator<Position> it = squares.iterator();
		while (it.hasNext()) {
			Position coord = it.next();
			grid.getSquare(coord.getRow(), coord.getCol()).setValue(Square.PATH);
		}
		assertEquals("--···---·#", printer.print(grid).toString());
	}

	@Test
	public void testPlacement_rook_2() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
		
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		
		Piece piece = new Rook();
		PlacementInfo placementInfo = piece.getPlacementInfo(grid, 0, 1);
		assertTrue(placementInfo.isSuccess());
		List<Position> squares = placementInfo.getBoundaries();
		Iterator<Position> it = squares.iterator();
		while (it.hasNext()) {
			Position coord = it.next();
			grid.getSquare(coord.getRow(), coord.getCol()).setValue(Square.PATH);
		}
		assertEquals("·-·-·--·-#", printer.print(grid).toString());
	}
	
	@Test
	public void testPlacement_king() {
		BoardPrinter printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
		
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		
		Piece piece = new King();
		PlacementInfo placementInfo = piece.getPlacementInfo(grid, 1, 0);
		assertTrue(placementInfo.isSuccess());
		List<Position> squares = placementInfo.getBoundaries();
		Iterator<Position> it = squares.iterator();
		while (it.hasNext()) {
			Position coord = it.next();
			grid.getSquare(coord.getRow(), coord.getCol()).setValue(Square.PATH);
		}
		assertEquals("··--·-··-#", printer.print(grid).toString());
	}

}
