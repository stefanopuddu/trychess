package com.spuddu.trychess.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.spuddu.trychess.model.piece.King;
import com.spuddu.trychess.model.piece.Queen;
import com.spuddu.trychess.model.piece.Rook;
import com.spuddu.trychess.util.print.board.BoardPrinter;
import com.spuddu.trychess.util.print.board.BoardPrinterFactory;

public class TestBoard {

	private BoardPrinter printer;
	
	@Before
	public void setUp() {
		printer = BoardPrinterFactory.getPrinter(BoardPrinter.INLINE);
	}
	
	
	@Test
	public void testBoard() {
		Board board = new Board(3,3);
		assertEquals(3, board.getNumRows());
		assertEquals(3, board.getNumColumns());
	}

	@Test
	public void testGrid() {
		Board board = new Board(5,3);
		Grid grid = board.getGridInstance();
		
		assertEquals(5, grid.getNumRows());
		assertEquals(3, grid.getNumColumns());
		
		board = new Board(2,3);
		grid = board.getGridInstance();
		
		assertEquals(2, grid.getNumRows());
		assertEquals(3, grid.getNumColumns());
	}
	
	@Test
	public void testNextFreePosition() {
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		PlacementInfo placementInfo1 = new King().getPlacementInfo(grid, 0, 0);
		grid = board.put(grid, placementInfo1);
		
		assertEquals(new Position(0,2), board.getNextFreePosition(grid, 0, 0));
		assertEquals(new Position(0,2), board.getNextFreePosition(grid, 0, 1));
		assertEquals(new Position(1,2), board.getNextFreePosition(grid, 0, 2));
		assertEquals(new Position(1,2), board.getNextFreePosition(grid, 1, 1));
		assertEquals(new Position(2,0), board.getNextFreePosition(grid, 1, 2));
		assertEquals(new Position(0,2), board.getNextFreePosition(grid, 2, 2));
		
		List<Position> freePositions = board.getAllFreePositions(grid);
		assertEquals(5, freePositions.size());
		assertTrue(freePositions.contains(new Position(0, 2)));
		assertTrue(freePositions.contains(new Position(1, 2)));
		assertTrue(freePositions.contains(new Position(2, 0)));
		assertTrue(freePositions.contains(new Position(2, 1)));
		assertTrue(freePositions.contains(new Position(2, 2)));
	}

	@Test
	public void testNextFreePosition_2() {
		Board board = new Board(5,5);
		Grid grid = board.getGridInstance();
		PlacementInfo placementInfo1 = new Rook().getPlacementInfo(grid, 0, 0);
		grid = board.put(grid, placementInfo1);
		PlacementInfo placementInfo2 = new Rook().getPlacementInfo(grid, 1, 1);
		grid = board.put(grid, placementInfo2);
		PlacementInfo placementInfo3 = new Rook().getPlacementInfo(grid, 2, 2);
		grid = board.put(grid, placementInfo3);
		PlacementInfo placementInfo4 = new Rook().getPlacementInfo(grid, 3, 3);
		grid = board.put(grid, placementInfo4);
		assertEquals(new Position(4,4), board.getNextFreePosition(grid, 0, 0));
		assertEquals(new Position(4,4), board.getNextFreePosition(grid, 4, 4));

		PlacementInfo placementInfo5 = new Rook().getPlacementInfo(grid, 4, 4);
		grid = board.put(grid, placementInfo5);
		
		assertNull(board.getNextFreePosition(grid, 4, 4));
		assertNull(board.getNextFreePosition(grid, 0, 0));
		assertNull(board.getNextFreePosition(grid, 3, 0));
	}


	@Test
	public void testPut() {
		
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		PlacementInfo placementInfo1 = new King().getPlacementInfo(grid, 0, 0);
		PlacementInfo placementInfo2 = new King().getPlacementInfo(grid, 0, 2);
		assertEquals("---------#", printer.print(grid).toString());
		Grid newgrid = board.put(grid, placementInfo1);
		assertEquals("---------#", printer.print(grid).toString());
		assertEquals("K·-··----#", printer.print(newgrid).toString());
		newgrid = board.put(newgrid, placementInfo2);
		PlacementInfo placementInfo = board.findPosition(newgrid, new Rook(),2,0);
		if (null != placementInfo && placementInfo.isSuccess()) {			
			newgrid = board.put(newgrid, placementInfo);
		}
		assertEquals("K·K····R·#", printer.print(newgrid).toString());
	}

	@Test
	public void testInputPermutations() {
		Board board = new Board(3,3);
		String input = "abc";
		Set<String> perm = board.getInputPermutations(input);
		assertEquals(6, perm.size());
	}

	@Test
	public void testGetAllPlacementInfo_1() {
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		assertEquals(3, board.getNumRows());
		assertEquals(3, board.getNumColumns());
		int row = 1;
		int col = 1;
		Piece piece = new King();
		
		List<PlacementInfo> list = board.getAllPlacementInfo(grid, piece, row, col);
		assertEquals(9, list.size());
		
		PlacementInfo next = board.findPosition(grid, piece, row, col);
		assertNotNull(next);
		assertEquals(next.getPosition(), list.get(0).getPosition());
		
		assertEquals(next.getPiece().getSymbol(), list.get(0).getPiece().getSymbol());
		for (int i = 0; i < next.getBoundaries().size(); i++) {
			assertEquals(next.getBoundaries().get(i), list.get(0).getBoundaries().get(i));
		}
		
	}

	@Test
	public void testGetAllPlacementInfo_2() {

		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		assertEquals(3, board.getNumRows());
		assertEquals(3, board.getNumColumns());
		assertEquals("---------#", printer.print(grid).toString());
		int row = 1;
		int col = 1;
		PlacementInfo p = new Queen().getPlacementInfo(grid, row, col);
		grid = board.put(grid, p);
		assertEquals("····Q····#", printer.print(grid).toString());
		List<PlacementInfo> list = board.getAllPlacementInfo(grid, new Rook(), row, col);
		assertEquals(0, list.size());
	}

	@Test
	public void testGetAllPlacementInfo_3() {
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		assertEquals(3, board.getNumRows());
		assertEquals(3, board.getNumColumns());
		
		assertEquals("---------#", printer.print(grid).toString());
		int row = 0;
		int col = 1;
		PlacementInfo p1 = new Queen().getPlacementInfo(grid, row, col);
		grid = board.put(grid, p1);
		assertEquals("·Q····-·-#", printer.print(grid).toString());

		Piece piece = new King();
		List<PlacementInfo> list = board.getAllPlacementInfo(grid, piece, row, col);
		assertEquals(2, list.size());
		
		PlacementInfo next = board.findPosition(grid, piece, row, col);
		assertNotNull(next);
		assertEquals(next.getPosition(), list.get(0).getPosition());
		assertEquals(next.getPiece().getSymbol(), list.get(0).getPiece().getSymbol());
		for (int i = 0; i < next.getBoundaries().size(); i++) {
			assertEquals(next.getBoundaries().get(i), list.get(0).getBoundaries().get(i));
		}
	}

	@Test
	public void testGetAllPlacementInfo_4() {
		Board board = new Board(3,3);
		Grid grid = board.getGridInstance();
		assertEquals(3, board.getNumRows());
		assertEquals(3, board.getNumColumns());
		
		int row = 0;
		int col = 1;
		PlacementInfo p1 = new Queen().getPlacementInfo(grid, row, col);
		grid = board.put(grid, p1);
		
		PlacementInfo p2 = new King().getPlacementInfo(grid, 2, 0);
		grid = board.put(grid, p2);

		PlacementInfo p3 = new King().getPlacementInfo(grid, 2, 2);
		grid = board.put(grid, p3);

		assertEquals("·Q····K·K#", printer.print(grid).toString());
		
		List<PlacementInfo> list = board.getAllPlacementInfo(grid, new King(), row, col);
		assertEquals(0, list.size());
	}

	@Test
	public void testGetAllPlacementInfo_5() {
		Board board = new Board(7,7);
		Grid grid = board.getGridInstance();
		
		int row = 0;
		int col = 1;
		PlacementInfo p1 = new King().getPlacementInfo(grid, 6, 6);
		grid = board.put(grid, p1);
		
		PlacementInfo p2 = new King().getPlacementInfo(grid, 0, 0);
		grid = board.put(grid, p2);
		
		List<PlacementInfo> list = board.getAllPlacementInfo(grid, new Queen(), row, col);
		assertEquals(20, list.size());
	}
	
}
