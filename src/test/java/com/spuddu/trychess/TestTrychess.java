package com.spuddu.trychess;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.kohsuke.args4j.CmdLineException;

public class TestTrychess {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testBlankInput() throws Throwable {
		thrown.expect(CmdLineException.class);
		thrown.expectMessage("Argument \"Board\" is required");
		String[] args = new String[] {};
		Trychess.main(args);
	}

	@Test
	public void testBlankInput_2() throws Throwable {
		thrown.expect(CmdLineException.class);
		thrown.expectMessage("Argument \"Pieces\" is required");
		String[] args = new String[] {""};
		Trychess.main(args);
	}

	@Test
	public void testWrongDimInput() throws Throwable {
		thrown.expect(Exception.class);
		thrown.expectMessage("\"x4\" is not a valid value for \"Board\"");
		String[] args =  new String[] {"x4", "KKR"};
		Trychess.main(args);
	}

	@Test
	public void testWrongSymbolInput() throws Throwable {
		thrown.expect(Exception.class);
		thrown.expectMessage("\"HKR\" is not a valid value for \"Pieces\"");
		String[] args =  new String[] {"3x3", "HKR"};
		Trychess.main(args);
	}

	@Test
	public void testWrongSymbolInput_2() throws Throwable {
		thrown.expect(Exception.class);
		thrown.expectMessage("\"K KR\" is not a valid value for \"Pieces\"");
		String[] args =  new String[] {"3x3", "K KR"};
		Trychess.main(args);
	}


	@Test
	public void testOK_wrongformat() throws Throwable {
		String testFolder = "target" + File.separator;
		String[] args =  new String[] {"-f", "wrong", "3x3", "KKR", "-o", testFolder + "kkr.txt"};
		Trychess.main(args);
		File file = new File(testFolder + "kkr.txt");
		assertTrue(file.exists());
		file.delete();
	}
	
	@Test
	public void testOK_file() throws Throwable {
		String testFolder = "target" + File.separator;
		String[] args =  new String[] {"-f", "inline", "3x3", "KKR", "-o",  testFolder + "kkr.txt"};
		Trychess.main(args);
		File file = new File(testFolder + "kkr.txt");
		assertTrue(file.exists());
		file.delete();
	}

}
